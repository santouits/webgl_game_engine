export { window_mock, document_mock, canvas_mock };

let window_mock = {};
let document_mock = {};
let canvas_mock = {};

document_mock.createElement = (name) => { return canvas_mock; };

canvas_mock.setAttribute = (name, value) => {};

import { webgl_mock } from "./gl_context_mock.mjs";
canvas_mock.getContext = (name) => { return webgl_mock; };
