import { webgl } from "./gl_context.mjs";

class Texture {
	constructor() {
		this.gl_texture = webgl.createTexture();
	}

	load_image(path) {
		// Check if image is already loaded somewhere TODO

		// Image is not loaded, load it
		// and add it to the cache TODO
		let image = new Image();
		image.onload = (function(gl_texture) {
			return function() {
				webgl.bindTexture(webgl.TEXTURE_2D, gl_texture);
				webgl.texImage2D(webgl.TEXTURE_2D, 0, webgl.RGBA, webgl.RGBA, webgl.UNSIGNED_BYTE, this);

				let coords = [
					0.0, 0.0,
					1.0, 0.0,
					1.0, 1.0,
					0.0, 1.0,
				];

				webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(coords), webgl.STATIC_DRAW);

			};
		})(this.gl_texture);
		image.src = path;
	}
}

export { Texture };
