import { webgl } from "./gl_context.mjs";

class ShaderProgram {
	constructor() {
		this.gl_program = webgl.createProgram();
	}

	link() {
		webgl.bindAttribLocation(this.gl_program, 0, 'a_position');
		webgl.bindAttribLocation(this.gl_program, 1, 'a_color');
		webgl.linkProgram(this.gl_program);
		if (!webgl.getProgramParameter(this.gl_program, webgl.LINK_STATUS)) {
			console.log('Unable to initialize the shader program: ' + webgl.getProgramInfoLog(this.gl_program));
			this.gl_program = null; // delete it first TODO
			return;
		}
	//	this.projection_location = webgl.getUniformLocation(this.gl_program, "projection");
		//this.model_view_location = webgl.getUniformLocation(this.gl_program, "model_view");
	//	console.log(this.projection_location);
	//	console.log(this.model_view_location);
//		console.log(this.vertex_pos_location);
	}

	attach_vertex_shader(vertex_shader) {
		this.vertex_shader = vertex_shader;
		webgl.attachShader(this.gl_program, vertex_shader.gl_shader);
	}

	attach_fragment_shader(fragment_shader) {
		this.fragment_shader = fragment_shader;
		webgl.attachShader(this.gl_program, fragment_shader.gl_shader);
	}

	get_projection_matrix_location() {


	}

	use() {
		webgl.useProgram(this.gl_program);

	}

}

class Shader {
	load_from_string(type, source) {
		this.gl_shader = webgl.createShader(type);
//		console.log(this.gl_shader);

		webgl.shaderSource(this.gl_shader, source);

		webgl.compileShader(this.gl_shader);

		if (!webgl.getShaderParameter(this.gl_shader, webgl.COMPILE_STATUS)) {
			console.log('An error occurred compiling the shaders: ' + webgl.getShaderInfoLog(this.gl_shader));
			webgl.deleteShader(this.gl_shader);
			this.gl_shader = null;
		}

	}

}

export { ShaderProgram, Shader };
