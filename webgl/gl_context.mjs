export let webgl = null;

export let init_webgl = function() {
	let canvas_width = "800";
	let canvas_height= "600";

	const canvas = document.createElement("canvas");
	canvas.setAttribute("width", canvas_width);
	canvas.setAttribute("height", canvas_height);
	document.body.appendChild(canvas);

	webgl = canvas.getContext("webgl");
}


