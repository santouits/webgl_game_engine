import Engine from "./modules/engine.mjs";

export class Main {

	run() {
		let engine = new Engine();
		engine.start();

	}

};

//import { Main } from "/main.mjs";
//let main = new Main();
//window.onload = main.run.bind(main);
