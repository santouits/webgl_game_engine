import { ditto, test, assert } from "/ditto.mjs";

import { Main } from "/main.mjs";
import { webgl_mock as webgl, init_webgl_mock as init_webgl } from "/mocks/gl_context_mock.mjs";

ditto("main class", () => {
	test("When not getting a webgl context", () => {
		let init_webgl = () => null;
		let webgl = null;
		let main = new Main();
		main.init_webgl = () => undefined;
		main.get_webgl = () => null;
		main.run();
	})
});


