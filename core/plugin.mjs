import Hook from "hooks.mjs"

/* Every plugin can have hooks that other plugins can tap */
class Plugin {

	constructor() {
		this.hooks = {};
		this.hooks.aHook = new Hook();
		this.hooks.anotherHook = new Hook();
	}

	
	init(thePluginThatCalledThis) {
		thePluginThatCalledThis.hooks.run.tap(this.run);	

	}

	run() {



	}







}
