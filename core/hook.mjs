
export default class Hook {
	constructor(args) {

		this.callbacks = [];

		
		this.args = args;
	}

	call() { 
		this.callbacks.forEach(({callbackFunc}) => callbackFunc.apply(undefined, arguments));
	}

	registerCallback(callbackName, callbackFunc) {
		this.callbacks.push({ callbackName, callbackFunc });
	}
}
