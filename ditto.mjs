/* A really simple testing library for the game engine for the browser */

/*
 * Using it like:
 *
 * ************************************
 *
 * import { ditto, test, assert } from "/ditto.mjs";
 *
 * ditto("file name maybe", () => {
 *   
 *   test("test name maybe", () => {
 *		 
 *		 // do initializations and stuff 
 *
 *	   assert("1" === 1);
 *
 *   });
 * });
 *
 * ************************************
 */

/* Set to true if you want testing to stop when a test fails */
let strict = false;

export let ditto = function(name, func) {
	console.log("Running : " + name);
	try {
		func.call();
	} 
	catch(e) {
		if (e == "assert failed") {
			if (strict) {
				throw e;
			}
		}
		else {
			throw e;
		}
	}
}

export let test = function(name, func, ditto) {
	console.log("  Running: " + name);
	try {
		func.call();
	} 
	catch(e) {
		if (e == "assert failed") {
			if (strict) {
				throw e;
			}
		}
		else {
			throw e;
		}
	}
}

export let assert = function(arg) {
	if (arg) {
		console.log("    passed");
		return true;
	} else {
		console.log("    failed")
		throw "assert failed";
	}
}

