import { webgl } from "../webgl/gl_context.mjs";

class Object2D {
	get_webgl() { return webgl; };
	
	constructor() {
		this.gl_buffer = this.get_webgl().createBuffer();
		this.vertices = [];
		this.shader = null;
		this.x = 0.0;
		this.y = 0.0;
		this.vertices_length = 0;
		this.gl_texture = null;
	}

	load_texture(path) {
		this.gl_texture = this.get_webgl().createTexture();

		let texture = this;
		let image = new Image();
	
		image.onload = function() {
			texture.get_webgl().bindTexture(texture.get_webgl().TEXTURE_2D, texture.gl_texture);
			texture.get_webgl().texImage2D(texture.get_webgl().TEXTURE_2D, 0, texture.get_webgl().RGBA, texture.get_webgl().RGBA, texture.get_webgl().UNSIGNED_BYTE, this);
			texture.get_webgl().texParameteri(texture.get_webgl().TEXTURE_2D, texture.get_webgl().TEXTURE_WRAP_S, texture.get_webgl().CLAMP_TO_EDGE);
			texture.get_webgl().texParameteri(texture.get_webgl().TEXTURE_2D, texture.get_webgl().TEXTURE_WRAP_T, texture.get_webgl().CLAMP_TO_EDGE);
			texture.get_webgl().texParameteri(texture.get_webgl().TEXTURE_2D, texture.get_webgl().TEXTURE_MIN_FILTER, texture.get_webgl().LINEAR);

			let vertices = [
				{
					position: [-1.0, 1.0],
					color: [1.0, 1.0, 1.0, 1.0],
					tex_coord: [0.0, 1.0]
				},
				{
					position: [1.0, 1.0],
					color: [1.0, 1.0, 1.0, 1.0],
					tex_coord: [1.0, 1.0]
				},
				{
					position: [-1.0, -1.0],
					color: [1.0, 1.0, 1.0, 1.0],
					tex_coord: [0.0, 0.0]
				},
				{
					position: [1.0, -1.0],
					color: [1.0, 1.0, 1.0, 1.0],
					tex_coord: [1.0, 0.0]
				}

			];

			texture.add_vertices(vertices);
		}
		image.src = path;
	}

	add_vertices(vertices) {
		this.vertices = vertices;
		this.vertices_length = this.vertices.length;

		//Create array buffer
		const buffer = new ArrayBuffer(32 * vertices.length);
		//Fill array buffer
		const dv = new DataView(buffer);
		for (let i = 0; i < vertices.length; i++) {
			// 2D Position
			dv.setFloat32(32 * i, vertices[i].position[0], true);
			dv.setFloat32(32 * i + 4, vertices[i].position[1], true);
			// Color
			dv.setFloat32(32 * i + 8, vertices[i].color[0], true);
			dv.setFloat32(32 * i + 12, vertices[i].color[1], true);
			dv.setFloat32(32 * i + 16, vertices[i].color[2], true);
			dv.setFloat32(32 * i + 20, vertices[i].color[3], true);
			// Texture Coordination
			dv.setFloat32(32 * i + 24, vertices[i].tex_coord[0], true);
			dv.setFloat32(32 * i + 28, vertices[i].tex_coord[1], true);

		}
		this.get_webgl().bindBuffer(this.get_webgl().ARRAY_BUFFER, this.gl_buffer);
		this.get_webgl().bufferData(this.get_webgl().ARRAY_BUFFER, buffer, this.get_webgl().STATIC_DRAW);
		this.get_webgl().bindBuffer(this.get_webgl().ARRAY_BUFFER, null);
	}

	clear_vertices() {
		this.vertices = [];
		this.vertices_length = 0;
	}

	delete_buffer() {
		this.get_webgl().deleteBuffer(this.gl_buffer);
		this.gl_buffer = null;
	}

	override_shaders(shader_program) {

	}
}

export { Object2D };

