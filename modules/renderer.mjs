import { webgl } from "/webgl/gl_context.mjs";

export let renderer = null;

export function init_renderer() {
	// TODO Check if renderer exists and throw error
	renderer = new Renderer();
}

class Renderer {
	constructor() {
		this.objects = [];

	}

	add_object(object_state) {
		this.objects.push(object_state);
	}

	get_webgl() { return webgl; }

	set_webgl_state(object_state) {
		this.get_webgl().bindBuffer(this.get_webgl().ARRAY_BUFFER, object_state.gl_buffer);
		this.get_webgl().vertexAttribPointer(0, 2, this.get_webgl().FLOAT, false, 32, 0);
		this.get_webgl().enableVertexAttribArray(0);
		this.get_webgl().vertexAttribPointer(1, 4, this.get_webgl().FLOAT, false, 32, 8);
		this.get_webgl().enableVertexAttribArray(1);
		this.get_webgl().vertexAttribPointer(2, 2, this.get_webgl().FLOAT, false, 32, 24);
		this.get_webgl().enableVertexAttribArray(2);

		if (object_state.gl_texture) {
			this.get_webgl().activeTexture(this.get_webgl().TEXTURE0);
			this.get_webgl().bindTexture(this.get_webgl().TEXTURE_2D, object_state.gl_texture);
		}
	}

	reset_webgl_state(object_state) {

	}

	draw_object(object_state) {
		this.set_webgl_state(object_state);
		this.get_webgl().drawArrays(this.get_webgl().TRIANGLE_STRIP, 0, object_state.vertices_length);
		this.reset_webgl_state(object_state);

	}

	draw() {
		this.get_webgl().clearColor(0.0, 0.0, 0.0, 1.0);
		this.get_webgl().clear(this.get_webgl().COLOR_BUFFER_BIT);

		this.objects.forEach((object_state) => {
			this.draw_object(object_state);
		}, this);
	}

}

