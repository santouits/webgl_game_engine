/* A default draw method for being called inside the main loop */

/* What a draw method should know hm */
// Find out how to make custom shaders from the beginning
import { webgl } from "../webgl/gl_context.mjs";
import { Object2D } from "../modules/object_2d.mjs";
import { Shader, ShaderProgram } from "../webgl/shader.mjs";
import { renderer, init_renderer } from "./renderer.mjs";

const A_POSITION = 0;
const A_NORMAL = 1;

class Draw {
	get_webgl() {
		return webgl;
	}

	get_renderer() {
		return renderer;
	}

	constructor() {
		init_renderer();

		// Create shaders
		const vertex_shader_string = `
			attribute vec2 a_position;
			attribute vec4 a_color;
			attribute vec2 a_tex_coord;
			varying vec4 v_color;
			varying vec2 v_tex_coord;

			void main() {
				gl_Position = vec4(a_position, 0.0, 1.0);
				v_color = a_color;
				v_tex_coord = a_tex_coord;
			}

		`;

		const fragment_shader_string = `
			precision mediump float;

			varying vec4 v_color;
			varying vec2 v_tex_coord;
			uniform sampler2D u_sampler;

			void main() {
				gl_FragColor = texture2D(u_sampler, v_tex_coord) * v_color;
			}
		`;

		let vertex_shader = new Shader();
		vertex_shader.load_from_string(webgl.VERTEX_SHADER, vertex_shader_string);
		let fragment_shader = new Shader();
		fragment_shader.load_from_string(webgl.FRAGMENT_SHADER, fragment_shader_string);
		let shader_program = new ShaderProgram();
		shader_program.attach_vertex_shader(vertex_shader);
		shader_program.attach_fragment_shader(fragment_shader);
		shader_program.link();

		// Create Objects
		let obj = new Object2D();
		let vertices = [
			{
				position: [-0.5, -0.5],
				color: [0.0, 0.0, 1.0, 1.0],
				tex_coord: [1.0, 1.0]
			},
			{
				position: [0.0,  0.5],
				color: [1.0, 0.0, 0.0, 1.0],
				tex_coord: [1.0, 1.0]
			},
			{
				position: [0.5, -0.5],
				color: [0.0, 1.0, 0.0, 1.0],
				tex_coord: [1.0, 1.0]
			}
		];
		obj.add_vertices(vertices);

		this.obj2 = new Object2D();
		let vertices2 = [
			{
				position: [0.9, 0.9],
				color: [0.0, 0.0, 1.0, 1.0],
				tex_coord: [1.0, 1.0]
			},
			{
				position: [0.9,  0.6],
				color: [1.0, 0.0, 0.0, 1.0],
				tex_coord: [1.0, 1.0]
			},
			{
				position: [0.6, 0.6],
				color: [0.0, 1.0, 0.0, 1.0],
				tex_coord: [1.0, 1.0]
			}
		];
		this.obj2.add_vertices(vertices2);


		this.obj3 = new Object2D();
		this.obj3.load_texture("./a.png");

		// Set opengl context

		shader_program.use();

		console.log(this.get_renderer());
		this.get_renderer().add_object(this.obj3);
		this.get_renderer().add_object(this.obj2);

		// Ready to draw

	}

	run() {
		this.get_renderer().draw();
	}


}


export { Draw };
