/* A default update method for the game engine */

export class Update {
	constructor() {
		this.time = 0.0;
	}

	run(delta) {
		this.time += delta;
		if (this.time > 10000) {
			this.time = 0.0;
		}
	}

}
