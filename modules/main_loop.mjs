/* A default main loop using the requestAnimationFrame method */

import { Draw } from "./draw.mjs";
import { Update } from "./update.mjs";
import Hook from "../core/hook.mjs";


export class MainLoop {
	constructor(engine) {
		
		this.hooks = {};
		this.hooks.init = new Hook();
	//	engine.hooks.run.registerCallback(this.run);
		engine.hooks.run.registerCallback("mainloop", this.run0.bind(this));

		this.draw = this.new_draw();
		this.update = this.new_update();
		this.previous_frame = 0;
	}

	new_draw() {
		return new Draw();
	}

	new_update() {
		return new Update();
	}

	get_window() {
		return window;
	}

	run0() {
		this.run(0);
	}

	run(now) {
		if (this.previous_frame === 0) {
			this.previous_frame = now;
			this.get_window().requestAnimationFrame(this.run.bind(this));
			return;
		}
		///////////////////////////////////////////////////////

		let delta = now - this.previous_frame;
		this.previous_frame = now;

		this.update.run(delta);
		

		this.draw.run();

		var id = this.get_window().requestAnimationFrame(this.run.bind(this));

	}
}
