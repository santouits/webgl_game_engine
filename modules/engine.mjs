import { webgl, init_webgl as _init_webgl } from "../webgl/gl_context.mjs";
import { MainLoop } from "../modules/main_loop.mjs";
import Hook from "../core/hook.mjs";


export default class Engine {
	constructor() {
		this.hooks = {};

		this.hooks.init = new Hook();
		this.hooks.run = new Hook();

		this.hooks.run.registerCallback("aa", (engine) => {
			console.log("hey from callback");
			console.log(engine);
		});
			
	}

	init() {
		this.hooks.init.call(this);
	}

	run() {
		this.hooks.run.call(this);
	}

	init_webgl() {
		_init_webgl();
	}

	get_webgl() {
		return webgl;
	}

	new_main_loop(engine) {
		return new MainLoop(engine);
	}

	start() {
		// Init webgl
		this.init_webgl();
		if (this.get_webgl() === null) {
			alert("Unable to initialize WebGL. Your browser or machine may not support it.");
			return;
		}

		// Load game

		let game = import("../editor/main.mjs");
		game.then(module => {
			console.log(module);
		});

		this.new_main_loop(this);

		this.run();



	}
};
