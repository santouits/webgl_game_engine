import { webgl } from "../webgl/gl_context.mjs";

/* I'm thinking of a manager that keeps all the shaders and gives them IDs */

export let shader_manager = null;

export function init_shader_manager() {
	shader_manager = new ShaderManager();
}

class ShaderManager {
	get_webgl() {
		return webgl;
	}

	register_shader(shader) {


	}

	constructor() {
		this.new_id = 0;

		// Create shaders
		const vertex_shader_string = `
			attribute vec2 a_position;
			attribute vec4 a_color;
			attribute vec2 a_tex_coord;
			varying vec4 v_color;
			varying vec2 v_tex_coord;

			void main() {
				gl_Position = vec4(a_position, 0.0, 1.0);
				v_color = a_color;
				v_tex_coord = a_tex_coord;
			}

		`;

		const fragment_shader_string = `
			precision mediump float;

			varying vec4 v_color;
			varying vec2 v_tex_coord;
			uniform sampler2D u_sampler;

			void main() {
				gl_FragColor = texture2D(u_sampler, v_tex_coord) * v_color;
			}
		`;

		let vertex_shader = new Shader();
		vertex_shader.load_from_string(webgl.VERTEX_SHADER, vertex_shader_string);
		let fragment_shader = new Shader();
		fragment_shader.load_from_string(webgl.FRAGMENT_SHADER, fragment_shader_string);
		let shader_program = new ShaderProgram();
		shader_program.attach_vertex_shader(vertex_shader);
		shader_program.attach_fragment_shader(fragment_shader);
		shader_program.link();


	}








}
